--
-- class Board
--

require('cell')
require('utils')

Board = {
    size = 3,
    grid = {},
    cellSize = nil,
}

function Board.Create(windowSize)
    local self = deepcopy(Board)
    self:init()
    self.cellSize = windowSize / 3
    return self
end

function Board:init()
    -- initialize grid
    for x=1, self.size do
        self.grid[x] = {}
        for y=1, self.size do
            self.grid[x][y] = Cell.Create()
        end
    end
end

function Board:draw(margin)
    local windowSize = self.cellSize * 3

    love.graphics.setColor(0,0,0,255)
    love.graphics.rectangle('fill', margin.x, margin.y, 1, windowSize)
    love.graphics.rectangle('fill', margin.x, margin.y, windowSize, 1)

    for x,v in pairs(self.grid) do
        --Vertical Grid Lines .1 of normal
        love.graphics.rectangle('fill', x * self.cellSize + margin.x, margin.y, 1, windowSize)

        for y,cell in pairs(v) do
            -- Draw cells
            cell:draw(
                self.cellSize,
                (x - 1) * self.cellSize + margin.x,
                (y - 1) * self.cellSize + margin.y
            )

            --Horizontal Grid lines
            love.graphics.rectangle('fill', margin.x, y * (self.cellSize) + margin.y, windowSize, 1)
        end
    end
end

function Board:getEmptyCells()
    local emptyCells = {}

    for x, col in pairs(self.grid) do
        for y,cell in pairs(col) do
            if cell:isEmpty() then
                table.insert(emptyCells, { x=x, y=y })
            end
        end
    end

    return emptyCells
end

function Board:isWonBy(user)
    local grid = self.grid

    --
    if (grid[1][1]:isOccupiedBy(user) and grid[1][2]:isOccupiedBy(user) and grid[1][3]:isOccupiedBy(user) or
        grid[2][1]:isOccupiedBy(user) and grid[2][2]:isOccupiedBy(user) and grid[2][3]:isOccupiedBy(user) or
        grid[3][1]:isOccupiedBy(user) and grid[3][2]:isOccupiedBy(user) and grid[3][3]:isOccupiedBy(user) or

        grid[1][1]:isOccupiedBy(user) and grid[2][1]:isOccupiedBy(user) and grid[3][1]:isOccupiedBy(user) or
        grid[1][2]:isOccupiedBy(user) and grid[2][2]:isOccupiedBy(user) and grid[3][2]:isOccupiedBy(user) or
        grid[1][3]:isOccupiedBy(user) and grid[2][3]:isOccupiedBy(user) and grid[3][3]:isOccupiedBy(user) or

        grid[1][1]:isOccupiedBy(user) and grid[2][2]:isOccupiedBy(user) and grid[3][3]:isOccupiedBy(user) or
        grid[3][1]:isOccupiedBy(user) and grid[2][2]:isOccupiedBy(user) and grid[1][3]:isOccupiedBy(user)) then
        return true
    else
        return false
    end
end
